# Learning_generalize_seizures_forecast

## Codes

On the codes folder we have the scripts we used to pre-process our data. In our case the counts of interictal spikes that we filled the gaps as in Proix et al., 2021 TLN. Then we extracted the broadband phase between 4-45 days. We use this data throughout the manuscript to forecast seizures.
In the codes folder we uploaded the codes we use to train the GLM/RNN models.

## RNN

In the folders savesRNN we have uploaded save of the 3 layer RNN model which can be downloaded and used to other datasets. To load the model:
'''

                name=('3L_Model_AES_papertd%.0fN%.0f_N2%0.0f_R%0.0f.json' %(timeD-1,Nodes,DenseN,ll)) 
                json_file = open(name, 'r')
                loaded_model_json = json_file.read()
                json_file.close()
                model = model_from_json(loaded_model_json)
                # load weights into new model                        
                name2=('3L_Model_weigth_AES_papertd%.0fN%.0f_N2%0.0f_R%0.0f.h5' %(timeD-1,Nodes,DenseN,ll)) 
                model.load_weights(name2)
                print("Loaded model from disk")
'''

The input for the RNN is a a matrix In = [time,backhistory,features] with a back history of 50 days and features using IEA,Cos,Sin (of the multidien phase) and the Seizures.


