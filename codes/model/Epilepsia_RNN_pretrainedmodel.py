# -*- coding: utf-8 -*-
"""
Created on Fri Sep 11 11:20:29 2020

@author: I0326672
"""

import numpy as np
import h5py, os, itertools, sys, matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Input, Dense, Dropout, Activation, Flatten, Convolution3D, Reshape, LSTM, Permute, GRU
from keras.regularizers import l2
from keras.optimizers import Adam, Adadelta
from keras.utils import np_utils
from sklearn.metrics import roc_auc_score, roc_curve, r2_score, auc
import scipy.io as sio
import pandas as pd # data processing
from tensorflow.keras.utils import plot_model
from keras.utils import to_categorical 
from tensorflow.keras.models import Model
import tkinter
import tensorflow as tf
from sklearn.metrics import mean_squared_error
from tensorflow.keras import layers
from scipy.stats import pearsonr
from math import pi
import math
import keras
from keras import backend as K
from scipy.io import savemat
from sklearn.metrics import confusion_matrix
from sklearn.metrics import brier_score_loss

""" LSTM """
if __name__ == '__main__':
    # path where the conccat matrices are
      # 
        path='C:/Users/I0326672/Documents/PostDoc/Seizure Forecasting/Generalized_seizures/data/saves/'
        os.chdir(path)
        timeD=50
        TimeF=0
#        listTimeD=[4,8,16,32,64]
        listTimeD=[5]

#        listTimeD=[50]
        Layers=[1]
        ListNodes=[40]
        resets=40
        Compiler=['binary_accuracy']
        Npat=159
        Loss=np.zeros(shape=(len(listTimeD),len(ListNodes),len(Layers)))
        LossV=np.zeros(shape=(len(listTimeD),len(ListNodes),len(Layers)))
        AurocP=np.zeros(shape=(Npat,resets))     
        BrierS=np.zeros(shape=(Npat,resets))    
        CorreTOT=np.zeros(shape=(Npat,len(listTimeD),2))      
        ratetrain=np.zeros(shape=Npat)
        Pval=np.zeros(shape=(Npat,resets))
        listrain = np.arange(0,Npat)
        AllList = np.zeros(shape=(Npat,resets))
        for j in range(resets):
            timeD=51
            path='C:/Users/I0326672/Documents/PostDoc/Seizure Forecasting/Generalized_seizures/data/saves/'
            os.chdir(path)
            name = ('DatasetSeiz_Phase_test_meanP_diffSubjectsD%.0fF%.0f.mat' %(timeD,TimeF))          
            annots = sio.loadmat(name)
            X=annots['DatasetTrain']
            Indtrain=annots['Patients']
            Npat=len(Indtrain[0,:])                    
            randpe = np.random.permutation(len(listrain))
            listtrainRand = listrain[randpe]
            AllList[:,j]=listtrainRand
            keras.backend.clear_session()
            # We now put all the subject train together
            xcosAll = []
            xsinAll = []
            IEAAll =[]
            SeizAll = []
            Seiz_test =[]
            X = np.array(X)
            for patli in range(round(.6*Npat)):
                    pat = listtrainRand[patli]
                    if pat==0:
                            ini=0
                            fin=Indtrain[0,0]

                    else:
                            ini=Indtrain[0,pat-1]
                            fin=Indtrain[0,pat]

                    if patli==0:
                            xcosAll=X[ini:fin,:50,2].copy()
                            xsinAll=X[ini:fin,:50,3].copy()
                            IEAAll=X[ini:fin,:50,0].copy()
                            SeizAll=X[ini:fin,:50,4].copy()
                            Seiz_test=X[ini:fin,50,4].copy()
                    else:       

                            xcosAll=np.concatenate((xcosAll,X[ini:fin,:50,2].copy()),axis=0)
                            xsinAll=np.concatenate((xsinAll,X[ini:fin,:50,3].copy()),axis=0)
                            IEAAll=np.concatenate((IEAAll,X[ini:fin,:50,0].copy()),axis=0)
                            SeizAll=np.concatenate((SeizAll,X[ini:fin,:50,4].copy()),axis=0)
                            Seiz_test=np.concatenate((Seiz_test,X[ini:fin,50,4].copy()),axis=0)
            Nodes=60
            DenseN=30
            xcosAll=np.expand_dims(xcosAll,-1)
            print(xcosAll.shape)
            xsinAll=np.expand_dims(xsinAll,-1)
            IEAAll=np.expand_dims(IEAAll,-1)
            SeizAll=np.expand_dims(SeizAll,-1)
            Seiz_test=np.array(Seiz_test)
            x_train = np.concatenate((IEAAll,xcosAll,xsinAll,SeizAll),axis=2)
            print(x_train.shape)
            tf.keras.backend.clear_session()
                
            input_layer = keras.layers.Input(shape=(timeD-1, 4))
            dense1 = keras.layers.LSTM(Nodes)(input_layer)
            dense2 = keras.layers.Dense(DenseN)(dense1)
            dense3 = keras.layers.Dense(15)(dense2)
            dropout_layer = Dropout(0.2)(dense3)
            output_layer = Dense(1,activation='sigmoid')(dropout_layer)
            model = Model(inputs=input_layer, outputs=output_layer)
            model.compile(loss='BinaryCrossentropy', optimizer='adam', metrics=['accuracy'])
            model.summary()      

            ini=0

            history=model.fit(x=x_train[:,:,:], y=Seiz_test[:],validation_split=0.15, batch_size=15000, epochs=30, verbose=1, shuffle=True)
            path='C:/Users/I0326672/Documents/PostDoc/Seizure Forecasting/Generalized_seizures/data/saves/Paper_saves/RNN_models/'
            os.chdir(path)
            model_json = model.to_json()
            name=('3L_Model_AES_papertd%.0fN%.0f_N2%0.0f_R%0.0f.json' %(timeD-1,Nodes,DenseN,j)) 
            with open(name, "w") as json_file:
                    json_file.write(model_json)
            name2=('3L_Model_weigth_AES_papertd%.0fN%.0f_N2%0.0f_R%0.0f.h5' %(timeD-1,Nodes,DenseN,j)) 
            model.save_weights(name2)
##            Nsurro=200
##            contAuc=0
##            for kk in range(round(.6*Npat), Npat):
##                    pat = listtrainRand[kk]
##                    if pat==0:
##                            ini=0
##                            fin=Indtrain[0,0]
##                    else:
##                            ini=Indtrain[0,pat-1]
##                            fin=Indtrain[0,pat]
##                    x_test = X[ini:fin,:50,[0,2,3,4]].copy()
##                    print(x_test.shape,ini,fin)
##                    y_pred =model.predict(x_test)
##                    y_test = X[ini:fin,timeD-1,4].copy()
##
###                           
##                    ini=fin
##                    contAuc=contAuc+1
            del model, X, Indtrain, Npat
##        np.save('Data_GRUmodeltd50__Nodes30_resetstd30_prova2','AurocP','BrierS','Pval')      
        mdic = {"AllList":AllList}
        savemat("ALL_list_Nodes60_30_3L.mat", mdic)
