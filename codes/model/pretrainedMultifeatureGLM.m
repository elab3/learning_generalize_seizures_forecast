clear all

%%%% Compute glm/glmm for %%%%
% load('Table_IEA_Seiz_Phase_withoutStrangePeaks.mat')
load('DatasetSeiz_Phase_test_meanP_diffSubjectsD51F0.mat');
Table_Calibration=[];
numL=40;
NumPat=159;
listTall=zeros(NumPat,numL);
 % prediction foward in days

coefall=zeros(3,numL);
coef_seizrate=zeros(numL,2);
AUCtot=nan(NumPat,3,numL);
% rng(1)
figure;
% numL numero the permutations in which we sort the patients
for l=1:numL
cosALL=[];
sinALL=[];
SeizTrain=[];
IEAALL=[];
SeizTALL=[];
listTrain=1:NumPat;
listT=listTrain(randperm(NumPat));
% listT=1:160;
listTall(:,l)=listT;
train=round(0.6*NumPat);
for i=1:train

if listT(i)==1
in=1;
fin=Patients(listT(i));
else
in=Patients(listT(i)-1)+1;
fin=Patients(listT(i));
end
CosP = DatasetTrain(in:fin,1:50,3);
SinP = DatasetTrain(in:fin,1:50,4);
IEA = DatasetTrain(in:fin,1:50,1);
Seiz = DatasetTrain(in:fin,1:50,5);
SeizT = DatasetTrain(in:fin,51,5);
cosALL=[cosALL;CosP];
sinALL=[sinALL;SinP];
SeizTrain=[SeizTrain;Seiz];
IEAALL=[IEAALL;IEA];
SeizTALL = [SeizTALL;SeizT];
end

mdl = fitglm([cosALL sinALL IEAALL SeizTrain],SeizTALL,'Distribution','poisson',...
'Link','log');
cont=0;
Nsurros=200;


for i=train+1:NumPat
cont=cont+1;
if listT(i)==1
in=1;
fin=Patients(listT(i));
else
in=Patients(listT(i)-1)+1;
fin=Patients(listT(i));
end

cosPtest=DatasetTrain(in:fin,1:50,3);
sinPtest=DatasetTrain(in:fin,1:50,4);
IEAtest=DatasetTrain(in:fin,1:50,1);
Seiztest=DatasetTrain(in:fin,1:50,5);

y_pred=predict(mdl,[cosPtest sinPtest IEAtest Seiztest]);

SS=DatasetTrain(in:fin,51,5);
SS(SS<0)=0;
SS(SS>=1)=1;
if numel(find(SS>=1))>1
[AUC,sensi,TiW]=AUC_timewarning(SS,y_pred);
AUCtot(cont,1,l)=AUC;
AUC_surro=zeros(Nsurros,1);

for k=1:Nsurros
y_predrand=y_pred(randperm(numel(y_pred)));
[AUCr,senser,TiWr]=AUC_timewarning(SS,y_predrand);
AUC_surro(k)=AUCr;


end
NumPosicioSurro=find(AUC_surro>AUC);
AUCtot(cont,2,l)=(numel(NumPosicioSurro)+1)/Nsurros;
[Probf,ProbO,BSS]=brier_skill_scoreC(y_pred,SS,'equal');
AUCtot(cont,3,l)=BSS;
% hold on
% plot(Probf,ProbO)
% 
% [Probfc,ProbOc,BSS]=brier_skill_scoreC(y_pred,SS,'equal');
% SSper=Table(listT(i),:).Seiz;
% SSper=SSper(1:trainL);
% SSper(SSper<0)=0;
% SSper(SSper>=1)=1;
% percseiz=numel(find(SSper>=1))/numel(SSper);
% 
% betas = mdl.Coefficients.Estimate;
% beta0=log(percseiz);
% yhat = exp(beta0*ones(numel(SS),1)+betas(2)*cosPtest(trainL:(end-1))+betas(3)*sinPtest(trainL:(end-1)));
% [Probfc,ProbOc,BSS]=brier_skill_scoreC(yhat,SS,'equal');
% 
Table_Calibration(listT(i)).Pred= y_pred;
Table_Calibration(listT(i)).Seizures = SS;
end

end
end

% 
% figure;boxplot([coefall(1,:)' ;coefall(2,:)' ;coefall(3,:)'],[ones(numL,1) ;2*ones(numL,1) ;3*ones(numL,1)])
% hold on
% scatter(0.1*randn(numL,1)+1,coefall(1,:),'ko','filled')
% hold on
% scatter(0.1*randn(numL,1)+2,coefall(2,:),'ko','filled')
% hold on
% scatter(0.1*randn(numL,1)+3,coefall(3,:),'ko','filled')

% %%%%
% figure;
% for l=1:numL
% [pID,pN] = FDR_correction(AUCtot(:,2,l),0.05);
% 
% bb = find(AUCtot(:,2,l)<=pID);
% bl = find(AUCtot(:,2,l)>pID);
% %  figure;
% %  boxplot([AUCtot(bb,1,l)],2*ones(numel(bb),1))
% % plot(l,numel(bb),'*')
% plot(l,mean(AUCtot(bb,1,l)),'*')
% hold on
% title(sprintf('L=%0.0f,Num =%0.0f',l,numel(bb)))
% end

% 5 realizations each subjects


newlistAUC=nan(NumPat,3,5);
for pat=1:NumPat
    cont=1;
    indx=1;
    while cont<6
        
    a=find(listTall((train+1):NumPat,indx)==pat);
    if ~isempty(a)
    newlistAUC(pat,:,cont)=AUCtot(a,:,indx);
    cont=cont+1;
    else
    
    end
    indx=indx+1;    
    end
    
end

end
[Probf,ProbO,BSS,Np]=brier_skill_scoreC(nonCal,allSeiz,'range');
