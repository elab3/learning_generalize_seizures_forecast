clear all

%%%% Compute glm/glmm for %%%%
% load('Table_IEA_Seiz_Phase_withoutStrangePeaks.mat')
load('Table_IEA_Seiz_Phase_meanPhase.mat');
Table_Calibration=[];
numL=40;
NumPat=159;

listTall=zeros(NumPat,numL);
 % prediction foward in days
figure;
timeF=1;
coefall=zeros(3,numL);
coef_seizrate=zeros(numL,2);
AUCtot=nan(159,3,numL);
rng(1)
Table(1)=[];
ALL_data_lenght = zeros(numL,2);
% numL numero the permutations in which we sort the patients
for l=1:numL
cosALL=[];
sinALL=[];
SeizALL=[];
IEAALL=[];
listTrain=1:NumPat;
listT=listTrain(randperm(NumPat));
% listT=1:160;
listTall(:,l)=listT;
train=round(0.6*NumPat);
for i=1:train
    
CosP = Table(listT(i),:).CosP_dw;
SinP = Table(listT(i),:).SinP_dw;
IEA = Table(listT(i),:).IEA;
Seiz = Table(listT(i),:).Seiz;
cosALL=[cosALL;CosP(1:end-timeF)];
sinALL=[sinALL;SinP(1:end-timeF)];
SeizALL=[SeizALL;Seiz((1+timeF):end)];
IEAALL=[IEAALL;IEA(1:end-timeF)];
end
ALL_data_lenght(l,1)=numel(SeizALL);
SeizALL(SeizALL>=1)=1;
mdl = fitglm([cosALL sinALL],SeizALL,'y ~1+ x1+x2','Distribution','poisson',...
'Link','log');
cont=0;
Nsurros=200;

coefall(:,l)=mdl.Coefficients.Estimate;
coef_seizrate(l,1)=mdl.Coefficients.Estimate(1);
coef_seizrate(l,2)=numel(find(SeizALL>=1))/numel(SeizALL);

for i=train+1:NumPat
cont=cont+1;
cosPtest=Table(listT(i),:).CosP_dw;
sinPtest=Table(listT(i),:).SinP_dw;
IEAtest=Table(listT(i),:).IEA;
if i==97
aaaLLOL=1;
end
trainL=min(round(0.6*numel(cosPtest)),480);
y_pred=predict(mdl,[cosPtest(trainL:(end-1)) sinPtest(trainL:(end-1))]);

ALL_data_lenght(l,2)=ALL_data_lenght(l,2)+numel(y_pred);

SS=Table(listT(i),:).Seiz;
SS=SS((trainL+1):end);
SS(SS<0)=0;
SS(SS>=1)=1;
if numel(find(SS>=1))>1
[AUC,sensi,TiW]=AUC_timewarning(SS,y_pred);
AUCtot(cont,1,l)=AUC;
AUC_surro=zeros(Nsurros,1);

for k=1:Nsurros
y_predrand=y_pred(randperm(numel(y_pred)));
[AUCr,sensir,TiWr]=AUC_timewarning(SS,y_predrand);
AUC_surro(k)=AUCr;


end
NumPosicioSurro=find(AUC_surro>AUC);
AUCtot(cont,2,l)=(numel(NumPosicioSurro)+1)/Nsurros;
[Probf,ProbO,BSS]=brier_skill_scoreC(y_pred,SS,'equal');
AUCtot(cont,3,l)=BSS;


[Probfc,ProbOc,BSS]=brier_skill_scoreC(y_pred,SS,'equal');
SSper=Table(listT(i),:).Seiz;
SSper=SSper(1:trainL);
SSper(SSper<0)=0;
SSper(SSper>=1)=1;
percseiz=numel(find(SSper>=1))/numel(SSper);

betas = mdl.Coefficients.Estimate;
beta0=log(percseiz);
yhat = exp(beta0*ones(numel(SS),1)+betas(2)*cosPtest(trainL:(end-1))+betas(3)*sinPtest(trainL:(end-1)));
[Probfc,ProbOc,BSS]=brier_skill_scoreC(yhat,SS,'equal');

Table_Calibration(listT(i)).Pred= y_pred;
Table_Calibration(listT(i)).Pred_calibrated = yhat;
Table_Calibration(listT(i)).Seizures = SS;
end

end
end

% 
% figure;boxplot([coefall(1,:)' ;coefall(2,:)' ;coefall(3,:)'],[ones(numL,1) ;2*ones(numL,1) ;3*ones(numL,1)])
% hold on
% scatter(0.1*randn(numL,1)+1,coefall(1,:),'ko','filled')
% hold on
% scatter(0.1*randn(numL,1)+2,coefall(2,:),'ko','filled')
% hold on
% scatter(0.1*randn(numL,1)+3,coefall(3,:),'ko','filled')

% %%%%
% figure;
% for l=1:numL
% [pID,pN] = FDR_correction(AUCtot(:,2,l),0.05);
% 
% bb = find(AUCtot(:,2,l)<=pID);
% bl = find(AUCtot(:,2,l)>pID);
% %  figure;
% %  boxplot([AUCtot(bb,1,l)],2*ones(numel(bb),1))
% % plot(l,numel(bb),'*')
% plot(l,mean(AUCtot(bb,1,l)),'*')
% hold on
% title(sprintf('L=%0.0f,Num =%0.0f',l,numel(bb)))
% end

% 5 realizations each subjects


newlistAUC=nan(NumPat,3,5);
for pat=1:NumPat
    cont=1;
    indx=1;
    while cont<6
        
    a=find(listTall(96:NumPat,indx)==pat);
    if ~isempty(a)
    newlistAUC(pat,:,cont)=AUCtot(a,:,indx);
    cont=cont+1;
    else
    
    end
    indx=indx+1;    
    end
    
end

%%%
sig = zeros(159,5);
for i=1:5
    
sigc = (newlistAUC(:,2,i)<=0.01);
sig(:,i)=double(sigc);

end
Allsig=sum(sig,2);
sigPat = find(Allsig==5);

Cal =[];
nonCal = [];
allSeiz =[];
for i=1:numel(sigPat)

noncalpred = Table_Calibration(sigPat(i)).Pred;
calpred = Table_Calibration(sigPat(i)).Pred_calibrated;
seiz = Table_Calibration(sigPat(i)).Seizures;

Cal = [Cal ; calpred];
nonCal = [nonCal ; noncalpred];
allSeiz = [allSeiz ; seiz];
end

figure;boxplot(coefall')
hold on
scatter(0.15*randn(1,40)+1,coefall(1,:),'k','filled')
hold on
scatter(0.15*randn(1,40)+2,coefall(2,:),'k','filled')
hold on
scatter(0.15*randn(1,40)+3,coefall(3,:),'k','filled')
