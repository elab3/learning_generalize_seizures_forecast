 function [amp, phase, fX] = acausal(X, muAll, option)

    % muAll: e.g. {[3 11 45] [6 12 30]}
    dt = 1/24;
    % Band of frequencies around the wavelet
    per = [.1:.05:1.3 , 1.4:.1:2, 2.2:.2:4 , 4.5:.5:10 , 11:1:50];
    fourier_factor = 1.0330;
    scales = per/fourier_factor;
    iCnt = 1;
    for imuAll = 1:length(muAll)
        mu = muAll{imuAll};

        for imu=1:length(mu)
            WT = cwtft({X(:, imuAll),dt},'scales', scales, 'wavelet','morl');
            bdw = 1/3*mu(imu);
            if strcmp(option, 'lumped')
                bd = per > mu(1) - bdw & per < mu(2) + bdw;
            
            elseif strcmp(option, 'broadband')
                
                bd = per > 5;
                
            else
                bd = per > mu(imu) - bdw & per < mu(imu) + bdw;
            end        
            WT.cfs(~bd,:)=0;
            fX(:, iCnt) = icwtft(WT)-mean(icwtft(WT));
            hX = hilbert(fX(:, iCnt));
            amp(:, iCnt) = abs(hX);
            phase(:, iCnt) = angle(hX);
            iCnt = iCnt + 1;
        end
    end
end