clear all; close all;
format LONGG

showPlot = 0;
saveData = 0;
filterType = 'broadband'; % 'acausal', 'lumped', 'causal0', 'causal1, 'causal2', 'broadband';
maxMultiRhythmPeriod = 50; % in days. Maximal perid of the multidien rhythms considered
gapInterpolationLimit = 10*24; % in hours. Bigger gaps are not interpolated
timeDayStart = 12;
timeDayEnd = 11;
lengthTrainParam = [480*24 0.60]; % [hours, percentage] for causal filter training, the minimal duration of train data is ...
% the minimum of minTrainData(1) in absolute number of days or minTrainData(2) in percentage of all data

% datasetc = 'UCSF_patients', %'trial_patients_any_sz_type', 'trial_patients', 'trial_patients_R0' 
pathData = ['C:\Users\I0326672\Documents\PostDoc\Seizure Forecasting\PointProcess\Data\NP DB (160) Forecast Redo\'];
pathProcessed = ['C:\Users\I0326672\Documents\PostDoc\Seizure Forecasting\PointProcess\Data\Preprocess'];
Dataset=[];
%% extract patients data from D structure
% Full list
%patientList = readmatrix([pathData 'patients.csv'], 'OutputType', 'char');
load('C:\Users\I0326672\Documents\PostDoc\Seizure Forecasting\PointProcess\Data\NP DB (160) Forecast Redo\Patient_List.mat');
patientList = Patient_List;
for idxPatient = 1:length(patientList)

    patient = patientList{idxPatient};
    disp(patient);
    stats{idxPatient}.name = patient;

    load([pathData patient])
    % to deal with different names in UCSF dataset for validated and not validated seizures
    if ~isfield(D.Selection.Hourly, 'C_sz')
        D.Selection.Hourly.C_sz = D.Selection.Hourly.E_sz;
    end
    % possible sanity check
    % assert(floor(size(D.Selection.Hourly.C_sz, 2)/24) + 1== size(D.Selection.Daily.C_sz,2), ...
    %            'the daily ts does not correspond to the hourly ts');
    if isfield(D.Selection, 'block2')
        disp('Two blocks for this patient');
    end
    if isfield(D.Selection, 'block3')
        MException('NotImplemented:notImplemented', ...
            'the code does not deal with more than two blocks in the data');
    end

    %% Get times series

    if size(strfind(patient, '2'), 1)==0
        [~, idxBlockHourlyt1] = min(abs(D.Selection.Hourly.Time-D.Selection.block1(1)));
        [~, idxBlockHourlyt2] = min(abs(D.Selection.Hourly.Time-D.Selection.block1(2)));
    else
        [~, idxBlockHourlyt1] = min(abs(D.Selection.Hourly.Time-D.Selection.block2(1)));
        [~, idxBlockHourlyt2] = min(abs(D.Selection.Hourly.Time-D.Selection.block2(2)));
    end

    % IEA time series
    IEAAll = transpose(D.Selection.Hourly.IEA1((idxBlockHourlyt1:idxBlockHourlyt2)));
    % if there is a second IEA time series
    boolTwoIEAs = isfield(D.Selection.Hourly, 'IEA2') && ...
                  sum(~isnan(D.Selection.Hourly.IEA2(idxBlockHourlyt1:idxBlockHourlyt2)))>0 && ...
                  sum(~isnan(D.Selection.Hourly.IEA1(idxBlockHourlyt1:idxBlockHourlyt2)))>0; 
    if boolTwoIEAs
        IEAAll = [IEAAll transpose(D.Selection.Hourly.IEA2(idxBlockHourlyt1:idxBlockHourlyt2))];
        assert(all(isnan(D.Selection.Hourly.IEA1(idxBlockHourlyt1:idxBlockHourlyt2))==isnan(D.Selection.Hourly.IEA2(idxBlockHourlyt1:idxBlockHourlyt2))), ...
               'NaNs are not the same in the two IEA time series');
    end

    % Seizure times series
    SzAll = transpose(D.Selection.Hourly.C_sz(idxBlockHourlyt1:idxBlockHourlyt2));

    % Time times series
    TimeAll = transpose(D.Selection.Hourly.Time(idxBlockHourlyt1:idxBlockHourlyt2));

    % select multidian peaks between 2 and 50 days
    Pks = D.Selection.pks_IEA1;
    peaksMultiAll = {Pks(Pks>=2 & Pks<maxMultiRhythmPeriod)};
    if boolTwoIEAs
        Pks = D.Selection.pks_IEA2;
        peaksMultiAll(2) = {Pks(Pks>=2 & Pks<maxMultiRhythmPeriod)};
    end
    nbPeaks = sum(cellfun(@(x) size(x, 2), peaksMultiAll));
    if strcmp(filterType, 'lumped')
        nbPeaks = 1;
    end
    stats{idxPatient}.peakMulti = peaksMultiAll;
      
    %% Divide data in blocks around large gaps
    % cut initial or trailing NaNs
    idxNotNan = find(~(isnan(IEAAll(:, 1)) | isnan(SzAll)));
    IEAAll =  IEAAll(idxNotNan(1):idxNotNan(end), :);  
    TimeAll = TimeAll(idxNotNan(1):idxNotNan(end));
    SzAll = SzAll(idxNotNan(1):idxNotNan(end));
    assert(~isnan(IEAAll(1, 1)), 'initial NaNs');
    assert(~isnan(IEAAll(end, 1)), 'trailing NaNs');

    % find large gaps
    idxGap = transpose(find(isnan(IEAAll(:, 1)) | isnan(SzAll))); %NaNs are the same in both IEA times series
    if length(idxGap)==0
        startBlock = [1];
        endBlock = [size(IEAAll, 1)];
    else
        endPosGap = [idxGap(diff(idxGap)>1) idxGap(end)];
        startPosGap = [idxGap(1) idxGap(find(diff(idxGap)>1)+1)];
        gapLength = endPosGap-startPosGap;
        stats{idxPatient}.gapLengths = gapLength;
        endBlock = [startPosGap(find(gapLength>gapInterpolationLimit))-1 size(IEAAll, 1)];
        startBlock = [1 endPosGap(find(gapLength>gapInterpolationLimit))+1];
    end

    tDiscAll = [];
    SzDiscFilledAll = [];
    IEADiscFilledAll = [];
    for iBlock = 1:length(startBlock)
        disp([datetime(TimeAll(startBlock(iBlock)), 'ConvertFrom', 'datenum') datetime(TimeAll(endBlock(iBlock)), 'ConvertFrom', 'datenum')]);

        iMarker = 0;
        for iIEA = 1:size(IEAAll, 2)
            IEA = IEAAll(startBlock(iBlock):endBlock(iBlock), iIEA);
            Sz = SzAll(startBlock(iBlock):endBlock(iBlock));
            Time = TimeAll(startBlock(iBlock):endBlock(iBlock));
            peaksMulti = peaksMultiAll{iIEA};

            assert(length(IEA)>50*24, 'block too small - check your data')

            if showPlot==1
                figure();
                sgtitle([patient ' raw data'])
                subplot(211)
                plot(24*(Time-Time(1)), IEA);
                xlim([0 length(Time)]);
                subplot(212);
                plot(24*(Time-Time(1)), Sz);
                xlim([0 length(Time)]);
            end
            
            %% Z-scoring the IEA data: Max did it already in the new data
            % tvisit = D.Settings.Visit;
            % indVisits = find(ismember(Time, tvisit));
            % tLast = 1;
            % zIEA = [];
            % for it=1:size(indVisits, 2)
            %     currDat = IEA(1, tLast:indVisits(it));
            %     zIEA = [zIEA (currDat - nanmean(currDat))/nanstd(currDat)];
            %     tLast = indVisits(it)+1;
            % end
            % currDat = IEA(1, tLast:end);
            % zIEA = [zIEA (currDat - nanmean(currDat))/nanstd(currDat)];
            zIEA = IEA;

            if showPlot==2
                figure();
                sgtitle([patient ' normalized data']) %2018b
                subplot(311)
                plot(24*(Time-Time(1)), IEA);
                xlim([0 length(Time)]);
                subplot(312)
                plot(24*(Time-Time(1)), zIEA);
                xlim([0 length(Time)]);        
                subplot(313);
                plot(24*(Time-Time(1)), Sz);
                xlim([0 length(Time)]);
            end
            
            %% We start and end all blocks at the time timeDayStart and timeDayEnd
            hStart = hour(dateshift(datetime(Time(1),'ConvertFrom','datenum'), 'start', 'hour', 'nearest'));
            if hStart<=timeDayStart
                tStart = timeDayStart - hStart + 1;
            elseif hStart>timeDayStart
                tStart = 24 + timeDayStart - hStart + 1;
            end
            hEnd = hour(dateshift(datetime(Time(end),'ConvertFrom','datenum'), 'start', 'hour', 'nearest'));
            if hEnd<timeDayEnd
                tEnd = length(Time) - (timeDayEnd + hEnd) - 2;
            elseif hEnd>=timeDayEnd
                tEnd = length(Time) - (hEnd - timeDayEnd);
            end
            % check
            assert(hour(dateshift(datetime(Time(tStart),'ConvertFrom','datenum'), 'start', 'hour', 'nearest'))==timeDayStart);
            assert(hour(dateshift(datetime(Time(tEnd),'ConvertFrom','datenum'), 'start', 'hour', 'nearest'))==timeDayEnd);

            if mod(tEnd-tStart + 1, 24)~=0
                disp(['Warning, some time ponts are missing in the times series, ' ...
                     'cutting so that the times series is a multiple of 24, '...
                     'the times series will not finish at the time asked for']);
                tEnd = tEnd - mod(tEnd-tStart + 1, 24);
            end

            IEADisc = zIEA(tStart:tEnd);
            SzDisc = Sz(tStart:tEnd);           
            tDisc = Time(tStart:tEnd);
            if iBlock==1
                startAll = tDisc(1); % initial start time
            end
            tvisit = D.Settings.Visit;
            indVisitsDisc = find(ismember(tDisc, tvisit))';
            tVisitUsedDisc = tDisc(ismember(tDisc, tvisit))';
            stats{idxPatient}.lengthtDisc = length(tDisc);
            
            if showPlot==3
                figure();
                sgtitle([patient ' IEA discarded']) %2018b
                subplot(211)
                plot(24*(tDisc-tDisc(1)), IEADisc);
                xlim([0 length(tDisc)]);
                subplot(212);
                plot(24*(tDisc-tDisc(1)), SzDisc);
                xlim([0 length(tDisc)]);
            end

            %% Filling small IEA gaps
            [IEADiscFilled, SzDiscFilled] = fillGaps(tDisc, IEADisc, SzDisc, peaksMulti, indVisitsDisc, tVisitUsedDisc, patient, showPlot);

            % stats     
            if iIEA==1
                if iBlock==1
                    stats{idxPatient}.lengthBlocks = length(IEADiscFilled);
                    stats{idxPatient}.nbSzBlocks = sum(SzDiscFilled>0);
                else
                    stats{idxPatient}.lengthBlocks = [stats{idxPatient}.lengthBlocks length(IEADiscFilled)];
                    stats{idxPatient}.nbSzBlocks = [stats{idxPatient}.nbSzBlocks sum(SzDiscFilled>0)];
                end
            end

            if showPlot==6
                figure();
                sgtitle([patient ' filled seizures'])
                subplot(211)
                plot(24*(tDisc-tDisc(1)), IEADiscFilled); hold on;
                plot(24*(tDisc-tDisc(1)), IEADisc);
                subplot(212);
                plot(24*(tDisc-tDisc(1)), SzDiscFilled); hold on;
                plot(24*(tDisc-tDisc(1)), SzDisc);
                
            end
            
            if iIEA==1
                IEADiscFilledBoth = IEADiscFilled;
            else
                IEADiscFilledBoth = [IEADiscFilledBoth IEADiscFilled];
            end
        end
        tDiscAll = [tDiscAll; tDisc];
        SzDiscFilledAll = [SzDiscFilledAll; SzDiscFilled];
        IEADiscFilledAll = [IEADiscFilledAll; IEADiscFilledBoth];
    end

    %% Now reestimate the rhythms with the filled data for the covariates:
    lengthTrainData = min([lengthTrainParam(1), floor(lengthTrainParam(2) * length(tDiscAll))]);

    if strcmp(filterType, 'acausal')

        [ampC, phaseC, filtSigC] = acausal(IEADiscFilledAll, repmat({[1]}, 1, size(IEADiscFilledAll, 2)), 'separated');
        [ampM, phaseM, filtSigM] = acausal(IEADiscFilledAll, peaksMultiAll, 'separated');
        
    elseif strcmp(filterType, 'broadband')
        
        [ampC, phaseC, filtSigC] = acausal(IEADiscFilledAll, repmat({[1]}, 1, size(IEADiscFilledAll, 2)), 'separated');
        [ampM, phaseM, filtSigM] = acausal(IEADiscFilledAll, repmat({[1]}, 1, size(IEADiscFilledAll, 2)), 'broadband');

    elseif strcmp(filterType, 'lumped')

        [ampC, phaseC, filtSigC] = acausal4(IEADiscFilledAll, repmat({[1]}, 1, size(IEADiscFilledAll, 2)), 'separated');
        [ampM, phaseM, filtSigM] = acausal4(IEADiscFilledAll, peaksMultiAll, 'lumped');

    elseif strcmp(filterType, 'causal0')   

        [ampC, phaseC, filtSigC] = causal0(IEADiscFilledAll, repmat({[1]}, 1, size(IEADiscFilledAll, 2)));
        [ampM, phaseM, filtSigM] = causal0(IEADiscFilledAll, peaksMultiAll);
    end


    if showPlot==7
        figure()
        plot(IEADiscFilledAll); hold on;
        plot(filtSigC); 
        yyaxis right
        sgtitle([patient ' circadian filtered IEA'])
        figure()
        plot(IEADiscFilledAll); hold on;
        plot(filtSigM); 
        yyaxis right
        sgtitle([patient ' multidien filtered IEA'])
    end

    if showPlot==8
        figure()
        plot(IEADiscFilledAll); hold on;
        plot(cos(phaseC));
        plot(ampC); 
        yyaxis right
        sgtitle([patient ' circadian abs and cos IEA'])
        figure()
        plot(IEADiscFilledAll); hold on;
        plot(cos(phaseM));
        plot(ampM);
        yyaxis right
        sgtitle([patient ' multidien abs and cos IEA'])
    end


    procDatHour = table();
    procDatDay = table();
    procDatHour.Time = tDiscAll;
    procDatDay.Time = mean24(tDiscAll);
    procDatHour.Seizures = SzDiscFilledAll;
    procDatDay.Seizures = mean24(SzDiscFilledAll)*24;
    procDatHour.IEA = IEADiscFilledAll;
    procDatDay.IEA = mean24(IEADiscFilledAll);
    for iIEA = 1:size(filtSigC, 2)
        procDatHour.(['CircadianAbs' num2str(iIEA)]) = ampC(:, iIEA);
        procDatDay.(['CircadianAbs' num2str(iIEA)]) = mean24(ampC(:, iIEA));
        procDatHour.(['CircadianAngle' num2str(iIEA)]) = phaseC(:, iIEA);
        procDatDay.(['CircadianAngle' num2str(iIEA)]) = mean24(phaseC(:, iIEA));
        procDatHour.(['CircadianCos' num2str(iIEA)]) = cos(phaseC(:, iIEA));
        procDatDay.(['CircadianCos' num2str(iIEA)]) = cos(mean24(phaseC(:, iIEA)));
        procDatHour.(['CircadianSin' num2str(iIEA)]) = sin(phaseC(:, iIEA));
        procDatDay.(['CircadianSin' num2str(iIEA)]) = sin(mean24(phaseC(:, iIEA)));
    end
    for iRhythm = 1:size(filtSigM, 2)
        procDatHour.(['MultidienAbs' num2str(iRhythm)]) = ampM(:, iRhythm);
        procDatDay.(['MultidienAbs' num2str(iRhythm)]) = mean24(ampM(:, iRhythm));
        procDatHour.(['MultidienAngle' num2str(iRhythm)]) = phaseM(:, iRhythm);
        procDatDay.(['MultidienAngle' num2str(iRhythm)]) = mean24(phaseM(:, iRhythm));
        procDatHour.(['MultidienCos' num2str(iRhythm)]) = cos(phaseM(:, iRhythm));
        procDatDay.(['MultidienCos' num2str(iRhythm)]) = cos(mean24(phaseM(:, iRhythm)));
        procDatHour.(['MultidienSin' num2str(iRhythm)]) = sin(phaseM(:, iRhythm));
        procDatDay.(['MultidienSin' num2str(iRhythm)]) = sin(mean24(phaseM(:, iRhythm)));
    end

        
    %% Time series for seizure circadian distribution
    % only use the train data) to estimate the distribution
    meanSzHourOfDay = zeros(24, 1);
    stdSzHourOfDay = zeros(24, 1);
    for hourOfDay = 0:23
        idxHour = find(mod(tDiscAll(1:lengthTrainData)*24, 24)==hourOfDay);
        meanSzHourOfDay(hourOfDay+1) = nanmean(SzDiscFilledAll(idxHour));
        stdSzHourOfDay(hourOfDay+1) = nanstd(SzDiscFilledAll(idxHour));
    end
    
    % Build the Circadian time series Sz
    tsMeanSzCirc = nan(length(SzDiscFilledAll), 1);
    tsStdSzCirc = nan(length(SzDiscFilledAll), 1);
    for iSzDisc = 1:length(SzDiscFilledAll)
        tsMeanSzCirc(iSzDisc) = meanSzHourOfDay(mod(tDiscAll(iSzDisc)*24, 24)+1);
        tsStdSzCirc(iSzDisc) = stdSzHourOfDay(mod(tDiscAll(iSzDisc)*24, 24)+1);
    end
    % tsMeanSzCirc(end) =  meanSzHourOfDay(mod(tDiscAll(end)*24, 24)+1);
    % tsStdSzCirc(end) =  stdSzHourOfDay(mod(tDiscAll(end)*24, 24)+1);

    if find(isnan(tsMeanSzCirc))
        disp("*******************NaN values left in circ Sz!!! ****************")
    end     

    if showPlot==9
        h = figure();
        sgtitle([patient ' circadian seizures'])
        subplot(411)
        plot(meanSzHourOfDay);
        subplot(412)
        plot(stdSzHourOfDay);
        subplot(413)
        plot(tsMeanSzCirc); hold on;
        plot(SzDiscFilledAll/max(SzDiscFilledAll(:)));
        subplot(414)
        plot(tsStdSzCirc); hold on;
        plot(SzDiscFilledAll/max(SzDiscFilledAll(:)));
    end

    % add circadian rhythms, no circadian rhythms for daily
    procDatHour.meanDailySeizures = tsMeanSzCirc;
    procDatHour.stdDailySeizures = tsStdSzCirc;


    %% Weekly distribution for Sz
    % only keep the first 60% (train data) to estimate the distribution
    idxEndTrain = floor(0.6 * length(tDiscAll));
    meanSzDayOfWeek = zeros(7, 1);
    stdSzDayOfWeek = zeros(7, 1);
    for dayOfWeek = 0:6
        idxDay = find(floor(mod(tDiscAll(1:idxEndTrain), 7))==dayOfWeek);
        meanSzDayOfWeek(dayOfWeek+1, 1) = nanmean(SzDiscFilledAll(idxDay));
        stdSzDayOfWeek(dayOfWeek+1, 1) = nanstd(SzDiscFilledAll(idxDay));
    end

    % Build the weekly time series Sz
    tsMeanSzWeek = nan(length(SzDiscFilledAll), 1);
    tsStdSzWeek = nan(length(SzDiscFilledAll), 1);
    for iSzDisc = 1:length(SzDiscFilledAll)
        tsMeanSzWeek(iSzDisc) = meanSzDayOfWeek(floor(mod(tDiscAll(iSzDisc), 7))+1);
        tsStdSzWeek(iSzDisc) = stdSzDayOfWeek(floor(mod(tDiscAll(iSzDisc), 7))+1);
    end


    if find(isnan(tsMeanSzWeek))
        disp("*******************NaN values left in week Sz!!! ****************")
    end     

    if showPlot==10
        h = figure();
        sgtitle([patient ' Weekly seizures'])
        subplot(411)
        plot(meanSzDayOfWeek);
        subplot(412)
        plot(stdSzDayOfWeek);
        subplot(413)
        plot(tsMeanSzWeek); hold on;
        plot(SzDiscFilledAll/max(SzDiscFilledAll(:)));
        subplot(414)
        plot(tsStdSzWeek); hold on;
        plot(SzDiscFilledAll/max(SzDiscFilledAll(:)));
    end

    % add weekly rhythms, only for daily
    procDatDay.meanWeeklySeizures = mean24(tsMeanSzWeek);
    procDatDay.stdWeeklySeizures = mean24(tsStdSzWeek);

    % plot concatenated data
    if showPlot==11
        figure();
        subplot(211);
        plot(procDatHour.Time, procDatHour.MultidienCos1); hold on;
        for isz = find(procDatHour.Seizures)
            scatter(procDatHour.Time(isz), procDatHour.MultidienCos1(isz), 'r', 'filled');
        end
        xlim([min(procDatHour.Time) max(procDatHour.Time)])
        subplot(212);
        plot(procDatHour.Time, procDatHour.IEA); hold on;
        scatter(procDatHour.Time(isz), procDatHour.IEA(isz), 'r', 'filled');
        sgtitle([patient ' concatenated'])
        xlim([min(procDatHour.Time) max(procDatHour.Time)])
    end

    % Finally collect some stats
    stats{idxPatient}.plvs(1) = {max(D.Selection.PLV_IEA1)};
    if boolTwoIEAs
        stats{idxPatient}.plvs(2) = {max(D.Selection.PLV_IEA2)};
    end
    stats{idxPatient}.sz_days = D.Selection.Daily.sz_days;

    if saveData
        if ~exist([pathProcessed 'features'], 'dir')
            mkdir([pathProcessed 'features']);
        end
        if ~exist([pathProcessed 'stats'], 'dir')
            mkdir([pathProcessed 'stats']);
        end
        writetable(procDatHour, [pathProcessed 'features/features_hour_' patient(3:4) '_' filterType '.csv']);
        writetable(procDatDay, [pathProcessed 'features/features_day_' patient(3:4) '_' filterType '.csv']);
        save([pathProcessed 'stats/stats_processing_' patient(3:4) '.csv']);
    end

    
    Table(idxPatient,1).NameP=idxPatient*ones(numel(mean24(SzDiscFilledAll)*24),1);
    Table(idxPatient,1).Seiz=(mean24(SzDiscFilledAll)*24);
    Table(idxPatient,1).Time=mean24(tDiscAll);
    Table(idxPatient,1).CosP=cos(mean24(phaseM(:, 1)));
    Table(idxPatient,1).SinP=sin(mean24(phaseM(:, 1)));
%     if numel(peaksMulti)==1
%     Table(idxPatient,1).CosP2=cos(mean24(phaseM(:, 1)));
%     Table(idxPatient,1).SinP2=sin(mean24(phaseM(:, 1)));
%     cos2=cos(mean24(phaseM(:, 1)));
%     sin2=sin(mean24(phaseM(:, 1)));
%     else
%     Table(idxPatient,1).CosP2=cos(mean24(phaseM(:, 2)));
%     Table(idxPatient,1).SinP2=sin(mean24(phaseM(:, 2)));
%     cos2=cos(mean24(phaseM(:, 1)));
%     sin2=sin(mean24(phaseM(:, 1)));
%     end
    Table(idxPatient,1).IEA=mean24(IEADiscFilledAll(:,1));
    Table(idxPatient,1).Phase=median24(phaseM(:,1));
    Table(idxPatient,1).CosP_dw = mean24(cos((phaseM(:,1))));
    Table(idxPatient,1).SinP_dw = mean24(sin((phaseM(:,1))));

    
    Dataset=[Dataset; [idxPatient*ones(numel(mean24(SzDiscFilledAll)*24),1) mean24(tDiscAll) mean24(IEADiscFilledAll(:,1)) (mean24(SzDiscFilledAll)*24) cos(mean24(phaseM(:, 1))) sin(mean24(phaseM(:, 1)))]]
    % timeDate = datestr(datetime(Time(ltStart(idxPatient):ltEnd(idxPatient)),'ConvertFrom','datenum'));
    % dlmwrite([pathProcessed 'rhythms/' patient '_time_date.csv'], char(cellstr(timeDate(:, 1:11))), '');  
    % dlmwrite([pathProcessed 'rhythms/' patient '_time_date_hours.csv'], char(cellstr(timeDate)), '');  
 
end
save('Table_IEA_Seiz_Phase_meanPhase.mat','Table')


% countBigGapPats = 0;
% countBigGaps = 0;
% for iPat = allLengthBigGaps
%     if ~isempty(iPat{1})
%         countBigGapPats = countBigGapPats + 1;
%         countBigGaps = countBigGaps + size(iPat{1}, 2);
%     end
% end

% countSmallGapPats = 0;
% countSmallGaps = 0;
% for iPat = allLengthSmallGaps
%     if ~isempty(iPat{1})
%         countSmallGapPats = countSmallGapPats + 1;
%         countSmallGaps = countSmallGaps + size(iPat{1}, 2);
%     end
% end

